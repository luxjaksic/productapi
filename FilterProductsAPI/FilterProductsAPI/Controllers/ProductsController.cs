﻿using FilterProductsAPI.Data;
using FilterProductsAPI.Models;
using FilterProductsAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Controllers
{
    /// <summary>
    /// Products controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IRepository<Product> repository;
        private readonly IPredicateService predicateService;
        private readonly IFilterObjectService filterObjectService;
        private readonly IHighlightService highlightService;
        private readonly ILogger<ProductsController> logger;

        /// <summary>
        /// Constructor for products controller.
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="predicateService">Predicate service</param>
        /// <param name="filterObjectService">Filter object service</param>
        /// <param name="highlightService">Highlight service</param>
        /// <param name="logger">Logger</param>
        public ProductsController(
            IRepository<Product> repository, 
            IPredicateService predicateService,
            IFilterObjectService filterObjectService,
            IHighlightService highlightService,
            ILogger<ProductsController> logger)
        {
            this.repository = repository;
            this.predicateService = predicateService;
            this.filterObjectService = filterObjectService;
            this.highlightService = highlightService;
            this.logger = logger;
        }

        /// <summary>
        /// Get products by parameters.
        /// If there are no parameters, all products will be returned.
        /// </summary>
        /// <remarks>
        /// Sample requests:
        /// GET /api/products - Returns all products
        /// GET /api/products?maxprice=20&size=medium - Returns all products that costs less than 20 and are medium size
        /// GET /api/products?size=medium, small - Returns all products that are in small or medium size
        /// GET /api/products?highlight=blue - Highlights all blue words in description
        /// GET /api/products?highlight=blue,red,green&size=small - Highlight a list of words and returns products in small size
        /// </remarks>
        /// <param name="parameters">
        /// Filter parameters are maximal price and a list of sizes.
        /// Highlight parameter is a list of word that will be highlighted.
        /// </param>
        /// <returns>Object that contains filtered highlighted products and filter object data</returns>
        [HttpGet]
        [SwaggerResponse(200, "Products returned succesfully.", typeof(Response))]
        [SwaggerResponse(400, "Bad request.")]
        [SwaggerResponse(404, "No products were found.")]
        public async Task<ActionResult<Response>> Get([FromQuery]FilterParameters parameters)
        {
            IEnumerable<Product> products;

            var infoSizes = parameters.Sizes != null ? string.Join<string>(",", parameters.Sizes) : "";
            var infoHighlights = parameters.Highlights != null ? string.Join<string>(",", parameters.Highlights) : " ";

            logger.LogInformation("Get products request for parameters maximal price = "
                + parameters.MaxPrice + ", sizes = " + infoSizes
                + ", highlights = " + infoHighlights);
            
            var predicate = predicateService.GetPredicate(parameters);

            if (predicate != null)
            {
                products = await repository.GetAllFilteredBy(predicate);
            }
            else
            {
                products = await repository.GetAll();
            }

            if (products == null || products.Count() == 0)
            {
                logger.LogWarning("No products were found for parameters maximal price = "
                    + parameters.MaxPrice + ", sizes = " + infoSizes
                    + ", highlights = " + infoHighlights);

                return NotFound("No products are found.");
            }

            var filteredObject = filterObjectService.CreateFilterObject(products);

            var highlightedProducts = highlightService.Highlight(products, parameters.Highlights);

            var response = new Response(highlightedProducts, filteredObject);

            return Ok(response); 
        }
    }
}
