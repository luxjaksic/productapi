using FilterProductsAPI.Attributes;
using FilterProductsAPI.Data;
using FilterProductsAPI.Models;
using FilterProductsAPI.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Reflection;

namespace FilterProductsAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

            services.AddScoped<IRepository<Product>, ProductRepository>();
            services.AddScoped<IPredicateService, PredicateService>();
            services.AddScoped<IFilterObjectService, FilterObjectService>();
            services.AddScoped<IMostCommonWordsService, MostCommonWordsService>();
            services.AddScoped<IHighlightService, HighlightService>();

            services.AddMvc(o =>
            {
                o.ValueProviderFactories.Insert(0, new SeparatedByCommasValueProviderFactory(","));
            });

            services.AddSwaggerGen(setupAction =>
            {
                setupAction.SwaggerDoc("FilterProductsAPI",
                    new Microsoft.OpenApi.Models.OpenApiInfo()
                    {
                        Title = "Filter Products API",
                        Version = "1",
                        Description = "This API allows you to access products, filter them by maximum price and sizes and higlight words in product description.",
                        Contact = new Microsoft.OpenApi.Models.OpenApiContact()
                        {
                            Email = "ljaksic95@gmail.com",
                            Name = "Lucija Jaksic"
                        }
                    });

                setupAction.EnableAnnotations();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                setupAction.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFile));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else if (env.IsProduction())
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();

            app.UseSwaggerUI(setupAction =>
            {
                setupAction.SwaggerEndpoint(
                    "/swagger/FilterProductsAPI/swagger.json",
                    "Filter Products API");
                setupAction.RoutePrefix = string.Empty;
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
