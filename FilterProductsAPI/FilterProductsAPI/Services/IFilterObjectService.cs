﻿using FilterProductsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Services
{
    public interface IFilterObjectService
    {
        public FilterObject CreateFilterObject(IEnumerable<Product> products);
    }
}
