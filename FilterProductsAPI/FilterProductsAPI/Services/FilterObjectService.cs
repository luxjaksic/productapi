﻿using FilterProductsAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace FilterProductsAPI.Services
{
    public class FilterObjectService : IFilterObjectService
    {
        private readonly IMostCommonWordsService mostCommonWords;

        public FilterObjectService(IMostCommonWordsService mostCommonWords)
        {
            this.mostCommonWords = mostCommonWords;
        }

        public FilterObject CreateFilterObject(IEnumerable<Product> products)
        {
            var descriptions = products.Select(p => p.Description);

            var filterObject = new FilterObject
            {
                MaximumPrice = products.Max(p => p.Price),
                MinimumPrice = products.Min(p => p.Price),
                Sizes = products
                    .SelectMany(p => p.Sizes)
                    .Distinct(),
                MostCommonWords = mostCommonWords.GetMostCommonWords(descriptions)
            };

            return filterObject;
        }
    }
}
