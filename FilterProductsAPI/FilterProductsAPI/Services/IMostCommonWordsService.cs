﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Services
{
    public interface IMostCommonWordsService
    {
        public IEnumerable<String> GetMostCommonWords(IEnumerable<string> descriptions, int take = 10, int skip = 5);
    }
}
