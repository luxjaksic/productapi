﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FilterProductsAPI.Services
{
    public class MostCommonWordsService : IMostCommonWordsService
    {
        public IEnumerable<string> GetMostCommonWords(IEnumerable<string> descriptions, int take = 10, int skip = 5)
        {
            var text = PrepareText(descriptions);

            var mostCommonWords = Regex.Split(text, @"\s+")
                .Where(x => x.Length != 0)
                .GroupBy(x => x)
                .Select(x => new
                {
                    Word = x.Key,
                    Count = x.Count()
                })
                .OrderByDescending(x => x.Count)
                .Skip(skip)
                .Take(take);

            return mostCommonWords.Select(x => x.Word);
        }

        private string PrepareText(IEnumerable<string> descriptions)
        {
            var text = string.Join(" ", descriptions);

            Regex rgx = new Regex("[^a-zA-Z]");
            text = rgx.Replace(text, " ");

            text = text.ToLower();

            return text;
        }
    }
}
