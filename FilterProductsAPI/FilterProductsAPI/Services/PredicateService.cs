﻿using FilterProductsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Services
{
    public class PredicateService : IPredicateService
    {
        public Func<Product, bool> GetPredicate(FilterParameters parameters)
        {
            Func<Product, bool> predicate = null;

            Func<Product, bool> maxPricePredicate = x => x.Price <= parameters.MaxPrice;
            Func<Product, bool> sizePredicate = x => parameters.Sizes.All(y => x.Sizes.Contains(y));

            if (parameters.MaxPrice != 0 
                && (parameters.Sizes != null && parameters.Sizes.Count() != 0))
            {
                predicate = x => maxPricePredicate(x) && sizePredicate(x);
            }
            else if (parameters.MaxPrice != 0)
            {
                predicate = maxPricePredicate;
            }
            else if (parameters.Sizes != null && parameters.Sizes.Count() != 0)
            {
                predicate = sizePredicate;
            }

            return predicate;
        }
    }
}
