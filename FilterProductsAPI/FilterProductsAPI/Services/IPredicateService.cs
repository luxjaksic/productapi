﻿using FilterProductsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Services
{
    public interface IPredicateService
    {
        public Func<Product, bool> GetPredicate(FilterParameters parameters);
    }
}
