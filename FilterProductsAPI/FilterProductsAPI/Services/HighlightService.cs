﻿using FilterProductsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FilterProductsAPI.Services
{
    public class HighlightService : IHighlightService
    {
        public IEnumerable<Product> Highlight(IEnumerable<Product> products, IEnumerable<string> highlights)
        {
            if (highlights == null || !highlights.Any())
            {
                return products;
            }

            var openingTag = "<em>";
            var closingTag = "</em>";

            foreach (var highlight in highlights) {
                var lowercaseHighlight = highlight.Trim().ToLower();

                foreach (var product in products)
                {
                    var description = product.Description.ToLower();
                    var indexOfOpeningTag = description.IndexOf(lowercaseHighlight);
                    var indexOfClosingTag = indexOfOpeningTag + lowercaseHighlight.Length + openingTag.Length;
                    
                    while (indexOfOpeningTag != -1)
                    {
                        product.Description = product.Description.Insert(indexOfOpeningTag, openingTag);
                        product.Description = product.Description.Insert(indexOfClosingTag, closingTag);

                        description = product.Description.ToLower();
                        indexOfOpeningTag = description.IndexOf(lowercaseHighlight, indexOfClosingTag);
                        indexOfClosingTag = indexOfOpeningTag + lowercaseHighlight.Length + openingTag.Length;
                    }
                }
            }

            return products;
        }
    }
}
