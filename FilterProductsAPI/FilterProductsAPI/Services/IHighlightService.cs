﻿using FilterProductsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Services
{
    public interface IHighlightService
    {
        public IEnumerable<Product> Highlight(IEnumerable<Product> products, IEnumerable<string> highlights);
    }
}
