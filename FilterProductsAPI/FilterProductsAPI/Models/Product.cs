﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Models
{
    /// <summary>
    /// Product model.
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Title of the product
        /// </summary>
        [JsonProperty(PropertyName = "title")]
        [MaxLength(200)]
        public string Title { get; set; }

        /// <summary>
        /// Price of product in unknown currency
        /// </summary>
        [JsonProperty(PropertyName = "price")]
        public double Price { get; set; }

        /// <summary>
        /// List of sizes the product is available in
        /// </summary>
        [JsonProperty(PropertyName = "sizes")]
        public IEnumerable<string> Sizes {get; set; }

        /// <summary>
        /// Description of product including higlighted words
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
    }
}
