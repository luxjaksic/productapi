﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Models
{
    /// <summary>
    /// Response contained of list of products and filter object.
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Constructor for response.
        /// </summary>
        /// <param name="products">List of products</param>
        /// <param name="filterObject">Filter object</param>
        public Response(IEnumerable<Product> products, FilterObject filterObject)
        {
            Products = products;
            FilterObject = filterObject;
        }

        /// <summary>
        /// List of products.
        /// </summary>
        public IEnumerable<Product> Products { get; set; }

        /// <summary>
        /// Filtered object containing info about minimal and maximal price, sizes and most common words.
        /// </summary>
        public FilterObject FilterObject { get; set; }
    }
}
