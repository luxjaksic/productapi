﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Models
{
    /// <summary>
    /// Data about products in response.
    /// </summary>
    public class FilterObject
    {
        /// <summary>
        /// Minimum price of all returned products.
        /// </summary>
        public double MinimumPrice { get; set; }
        /// <summary>
        /// Maximal price of all returned products.
        /// </summary>
        public double MaximumPrice { get; set; }
        /// <summary>
        /// List of sizes of all returned products.
        /// </summary>
        public IEnumerable<string> Sizes { get; set; }
        /// <summary>
        /// Ten most common words (first 5 most common skipped) in description of returned products.
        /// </summary>
        public IEnumerable<string> MostCommonWords { get; set; }
    }
}
