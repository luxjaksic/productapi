﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Models
{
    public class APIKeys
    {
        [JsonProperty(PropertyName = "primary")]
        public string Primary { get; set; }
        [JsonProperty(PropertyName = "secondary")]
        public string Secondary { get; set; }
    }
}
