﻿using FilterProductsAPI.Attributes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Models
{
    /// <summary>
    /// Contains parameters for filtering by maximal price or list of sizes.
    /// Contains parametar list of highlights to higlight words.
    /// </summary>
    public class FilterParameters
    {
        /// <summary>
        /// Filter by maximal price
        /// </summary>
        [FromQuery(Name = "maxprice")]
        public double MaxPrice { get; set; }

        /// <summary>
        /// Filter by list of sizes separated by comma
        /// </summary>
        [FromQuery(Name = "size")]
        [SeparatedByCommas]
        public IEnumerable<string> Sizes { get; set; }

        /// <summary>
        /// Highlight list of highlights separated by comma
        /// </summary>
        [FromQuery(Name = "highlight")]
        [SeparatedByCommas]
        public IEnumerable<string> Highlights { get; set; }
    }
}
