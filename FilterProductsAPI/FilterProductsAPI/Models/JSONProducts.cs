﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Models
{
    public class JSONProducts
    {
        [JsonProperty(PropertyName = "products")]
        public IEnumerable<Product> Products { get; set; }
        [JsonProperty(PropertyName = "apiKeys")]
        public APIKeys ApiKeys { get; set; }
    }
}
