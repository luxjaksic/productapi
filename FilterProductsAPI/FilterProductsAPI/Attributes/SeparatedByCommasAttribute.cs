﻿using System;

namespace FilterProductsAPI.Attributes
{
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class SeparatedByCommasAttribute : Attribute
    {

    }
}
