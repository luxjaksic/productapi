﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterProductsAPI.Attributes
{
    public class SeparatedByCommasValueProviderFactory : IValueProviderFactory
    { 
        private readonly string _separator;
        private readonly string _key;

        public SeparatedByCommasValueProviderFactory(string key, string separator)
        {
            _key = key;
            _separator = separator;
        }

        public SeparatedByCommasValueProviderFactory(string separator) : this(null, separator)
        {
        }

        public Task CreateValueProviderAsync(ValueProviderFactoryContext context)
        {
            context.ValueProviders.Insert(0, new SeparatedByCommasQueryStringValueProvider(_key, context.ActionContext.HttpContext.Request.Query, _separator));
            return Task.CompletedTask;
        } 
    }
}
