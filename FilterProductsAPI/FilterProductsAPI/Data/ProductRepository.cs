﻿using FilterProductsAPI.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading.Tasks;

namespace FilterProductsAPI.Data
{
    /// <summary>
    /// Product repository fetching data from mocky.io.
    /// </summary>
    public class ProductRepository : IRepository<Product>
    {
        private readonly string url = "http://www.mocky.io/v2/5e307edf3200005d00858b49";
        private readonly ILogger<ProductRepository> logger;

        /// <summary>
        /// Constructor for product repository.
        /// </summary>
        /// <param name="logger">Logger</param>
        public ProductRepository(ILogger<ProductRepository> logger)
        {
            this.logger = logger;
        }

        public async Task<IEnumerable<Product>> GetAll()
        {
            var JSONProducts = await GetAllFromMocky(url);

            return JSONProducts.Products;
        }

        public async Task<IEnumerable<Product>> GetAllFilteredBy(Func<Product, bool> predicate)
        {
            var products = await GetAll();

            products = products.Where(predicate);

            return products;
        }

        private async Task<JSONProducts> GetAllFromMocky(string url)
        {
            logger.LogInformation("Fech data form Mocky from url: " + url);

            using var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync(url);

            var models = JsonConvert.DeserializeObject<JSONProducts>(json);

            return models;
        }
    }
}
