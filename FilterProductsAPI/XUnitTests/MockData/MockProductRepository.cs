﻿using FilterProductsAPI.Data;
using FilterProductsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XUnitTests.MockData
{
    class MockProductRepository : IRepository<Product>
    {
        public readonly IEnumerable<Product> products = new List<Product>
        {
            new Product
            {
                Title = "First",
                Sizes = new List<string> {"small"},
                Price = 10,
                Description = "This is blue. This."
            },
            new Product
            {
                Title = "Second",
                Sizes = new List<string> {"medium"},
                Price = 20,
                Description = "This is red."

            },
            new Product
            {
                Title = "Third",
                Sizes = new List<string> {"large"},
                Price = 30,
                Description = "Blue is blue."

            },
            new Product
            {
                Title = "Fourth",
                Sizes = new List<string> {"large", "medium", "small"},
                Price = 40,                
                Description = "This is red and blue."

            },
            new Product
            {
                Title = "Fifth",
                Sizes = new List<string> {"large", "medium"},
                Price = 25,
                Description = "Blue is not red."

            }
        };

        public async Task<IEnumerable<Product>> GetAll()
        {
            return products;
        }

        public async Task<IEnumerable<Product>> GetAllFilteredBy(Func<Product, bool> predicate)
        {
            var filteredProducts = products.ToList().Where(predicate);

            return filteredProducts;
        }
    }
}
