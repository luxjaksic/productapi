﻿using FilterProductsAPI.Models;
using FilterProductsAPI.Services;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTests
{
    public class FilterObjectTests
    {
        [Fact]
        public void GivenProducts_WhenFilterObjectIsCreated_ThenFilterObjectIsCorrectlyFilled()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Title = "A Red Hat",
                    Price = 11,
                    Sizes = new List<string> {"small"},
                    Description = "This hat perfectly pairs with a green shoe."
                },
                 new Product
                {
                    Title = "A Green Trouser",
                    Price = 23,
                    Sizes = new List<string> {"small", "large"},
                    Description = "This trouser perfectly pairs with a blue belt."
                },
                  new Product
                {
                    Title = "This shirt perfectly pairs with a red hat.",
                    Price = 12,
                    Sizes = new List<string> {"medium"},
                    Description = "This shirt perfectly pairs with a green hat."
                },
            };

            var mostCommonWords = new List<string> { "shirt", "hat" };

            var mockMostCommonWords = new Mock<IMostCommonWordsService>();
            var service = new FilterObjectService(mockMostCommonWords.Object);
            mockMostCommonWords
                .Setup(x => x.GetMostCommonWords(It.IsAny<IEnumerable<string>>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(mostCommonWords);

            // Act
            var filterObject = service.CreateFilterObject(products);

            // Assert        
            Assert.NotNull(filterObject);
            Assert.Equal(23, filterObject.MaximumPrice);
            Assert.Equal(11, filterObject.MinimumPrice);

            var sizes = new List<string> { "small", "large", "medium" };
            Assert.True(filterObject.Sizes.All(s => sizes.Contains(s)));
        }
    }
}
