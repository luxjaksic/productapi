﻿using FilterProductsAPI.Models;
using FilterProductsAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTests
{
    public class PredicateServiceTests
    {
        public readonly IEnumerable<Product> products = new List<Product>
        { 
            new Product
            {
                Title = "First",
                Sizes = new List<string> {"small"},
                Price = 10
            },
            new Product
            {
                Title = "Second",
                Sizes = new List<string> {"medium"},
                Price = 20
            },
            new Product
            {
                Title = "Third",
                Sizes = new List<string> {"large"},
                Price = 30
            }
        };

        [Fact]
        public void GivenMaxPriceAsFilterParameter_WhenPredicateIsCalculatedAndProductsAreFilteredByIt_ExpectedResultAndActualResultAreEqual()
        {
            // Arange
            var service = new PredicateService();
            var filterParameters = new FilterParameters
            {
                MaxPrice = 12
            };

            Func<Product, bool> expectedPredicate = x => x.Price <= filterParameters.MaxPrice;

            // Act
            var predicate = service.GetPredicate(filterParameters);

            // Assert
            var expectedResult = products.Where(expectedPredicate);
            var actualResult = products.Where(predicate);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void GivenSizesFilterAsParameter_WhenPredicateIsCalculatedAndProductsAreFilteredByIt_ExpectedResultAndActualResultAreEqual()
        {
            // Arange
            var service = new PredicateService();
            var filterParameters = new FilterParameters
            {
                Sizes = new List<string> { "small", "large" }
            };

            Func<Product, bool> expectedPredicate = x => filterParameters.Sizes.All(y => x.Sizes.Contains(y));
            // Act
            var predicate = service.GetPredicate(filterParameters);

            // Assert
            var expectedResult = products.Where(expectedPredicate);
            var actualResult = products.Where(predicate);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void GivenMaxSizeAndSizesFilterAsParameter_WhenPredicateIsCalculatedAndProductsAreFilteredByIt_ExpectedResultAndActualResultAreEqual()
        {
            // Arange
            var service = new PredicateService();
            var filterParameters = new FilterParameters
            {
                MaxPrice = 20,
                Sizes = new List<string> { "small", "large" }
            };

            Func<Product, bool> maxPricePredicate = x => x.Price <= filterParameters.MaxPrice;
            Func<Product, bool> sizePredicate = x => filterParameters.Sizes.All(y => x.Sizes.Contains(y));

            Func<Product, bool> expectedPredicate = x => sizePredicate(x) && maxPricePredicate(x);

            // Act
            var predicate = service.GetPredicate(filterParameters);

            // Assert
            var expectedResult = products.Where(expectedPredicate);
            var actualResult = products.Where(predicate);

            Assert.Equal(expectedResult, actualResult);
        }
    }
}
