﻿using FilterProductsAPI.Services;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTests
{
    public class MostCommonWordsTests
    {
        [Fact]
        public void GivenAMostCommonWordWithDifferentUpperAndLowerLetters_WhenGetMostCommonWords_ReturnWordThatIsMostCommon()
        {
            // Arrange
            var service = new MostCommonWordsService();

            var description = new List<string>()
            {
                "Word",
                "Word word",
                "Top Top Top Top",
                "wOrd Word",
                "example"
            };

            // Act
            var words = service.GetMostCommonWords(description, take: 1, skip: 0);

            // Assert
            Assert.NotNull(words);
            Assert.Equal("word", words.Single().ToLower());
        }

        [Fact]
        public void GivenAMostCommonWordWithDifferentSpecialCharacters_WhenGetMostCommonWords_ReturnWordThatIsMostCommon()
        {
            // Arrange
            var service = new MostCommonWordsService();

            var description = new List<string>()
            {
                "Word",
                "Word, Word,",
                "Plus,    Plus!   Plus!",
                "Plus?"
            };

            // Act
            var words = service.GetMostCommonWords(description, take: 1, skip: 0);

            // Assert
            Assert.NotNull(words);
            Assert.Equal("plus", words.Single().ToLower());
        }

        [Fact]
        public void GivenDescriptionsWith11Words_WhenGet10MostCommonWordsWith5MostCommonSkipped_Return6MostCommonWords()
        {
            // Arrange
            var service = new MostCommonWordsService();
            const int NUMBER_OF_MOST_COMMON_WORDS = 6;

            var description = new List<string>()
            {
                "This trouser perfectly pairs with a green shirt.",
                "This trouser perfectly pairs with a green shirt.",
                "This shirt perfectly pairs with a red hat.",
                "This hat perfectly pairs with a red shoe.",
                "This hat perfectly pairs with a green shoe."
            };

            // Act
            var words = service.GetMostCommonWords(description);

            // Assert
            var expectedWords = new List<string>
            {
                "shirt", "trouser", "green", "red", "hat", "shoe"
            };

            Assert.Equal(NUMBER_OF_MOST_COMMON_WORDS, words.Count());
            Assert.True(words.All(x => expectedWords.Contains(x)));
        }
    }
}
