﻿using FilterProductsAPI.Models;
using FilterProductsAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace XUnitTests
{
    public class HighlightServiceTests
    {
        [Fact]
        public void GivenProductsWithHighlightedWordsBeingMentionedOnce_WhenHighlighted_ThenHighlightWordsBeingMentionedOnce()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Title = "A Red Hat",
                    Price = 11,
                    Sizes = new List<string> {"small"},
                    Description = "This hat perfectly pairs with a green shoe."
                },
                 new Product
                {
                    Title = "A Green Trouser",
                    Price = 23,
                    Sizes = new List<string> {"small", "large"},
                    Description = "This trouser perfectly pairs with a blue belt."
                },
                  new Product
                {
                    Title = "This shirt perfectly pairs with a red hat.",
                    Price = 12,
                    Sizes = new List<string> {"medium"},
                    Description = "This shirt perfectly pairs with a green hat."
                },
            };

            var highlights = new List<string> { "this", "with" };

            // Act
            var service = new HighlightService();

            var editedProducts = service.Highlight(products, highlights);

            // Assert
            var expectedDescriptions = new List<string>
            {
                "<em>This</em> hat perfectly pairs <em>with</em> a green shoe.",
                "<em>This</em> trouser perfectly pairs <em>with</em> a blue belt.",
                "<em>This</em> shirt perfectly pairs <em>with</em> a green hat."
            };
            Assert.True(editedProducts.All(x => expectedDescriptions.Contains(x.Description)));
        }

        [Fact]
        public void GivenProductsWithHighlightedWordsBeingMentionedTwice_WhenHighlighted_ThenHighlightWordsBeingMentionedTwice()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Title = "A Red Hat",
                    Price = 11,
                    Sizes = new List<string> {"small"},
                    Description = "This this hat perfectly pairs with a green shoe."
                },
                 new Product
                {
                    Title = "A Green Trouser",
                    Price = 23,
                    Sizes = new List<string> {"small", "large"},
                    Description = "This trouser perfectly pairs with a blue belt with a nice detail."
                },
                  new Product
                {
                    Title = "This shirt perfectly pairs with a red hat.",
                    Price = 12,
                    Sizes = new List<string> {"medium"},
                    Description = "This shirt perfectly pairs with a green hat."
                },
            };

            var highlights = new List<string> { "this", "with" };

            // Act
            var service = new HighlightService();

            var editedProducts = service.Highlight(products, highlights);

            // Assert
            var expectedDescriptions = new List<string>
            {
                "<em>This</em> <em>this</em> hat perfectly pairs <em>with</em> a green shoe.",
                "<em>This</em> trouser perfectly pairs <em>with</em> a blue belt <em>with</em> a nice detail.",
                "<em>This</em> shirt perfectly pairs <em>with</em> a green hat."
            };
            Assert.True(editedProducts.All(x => expectedDescriptions.Contains(x.Description)));
        }
    }
}
