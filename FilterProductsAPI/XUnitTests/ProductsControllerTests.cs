using FilterProductsAPI.Controllers;
using FilterProductsAPI.Data;
using FilterProductsAPI.Models;
using FilterProductsAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using XUnitTests.MockData;

namespace XUnitTests
{
    public class ProductsControllerTests
    {
        [Fact]
        public async Task GivenMaxPriceFilterParameter_WhenGetRequestIsSend_ReturnCorrectlyFilteredProductsWithCorrespondingObjectFilter()
        {
            // Arrange
            var maxPrice = 20;
            
            var productRepository = new MockProductRepository();

            var predicateService = new PredicateService();
            var mostCommonWordsService = new MostCommonWordsService();
            var filterObjectService = new FilterObjectService(mostCommonWordsService);
            var highlightService = new HighlightService();

            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .BuildServiceProvider();
            var factory = serviceProvider.GetService<ILoggerFactory>();
            var logger = factory.CreateLogger<ProductsController>();
             
            var productController = new ProductsController(
                productRepository,
                predicateService,
                filterObjectService,
                highlightService,
                logger);

            var filterParameters = new FilterParameters
            {
                MaxPrice = maxPrice
            };

            // Act
            var response = await productController.Get(filterParameters);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Response>>(response);
            var createdAtActionResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var returnValue = Assert.IsType<Response>(createdAtActionResult.Value);

            var actualProducts = returnValue.Products;
            var filterObject = returnValue.FilterObject;

            Assert.True(actualProducts.All(x => x.Price <= maxPrice));
            Assert.Equal(20, filterObject.MaximumPrice);
            Assert.Equal(10, filterObject.MinimumPrice);

            var expectedSizes = new List<string>() { "small", "medium" };
            Assert.True(expectedSizes.All(x => filterObject.Sizes.Contains(x)));
        }

        [Fact]
        public async Task GivenSizesFilterParameter_WhenGetRequestIsSend_ReturnCorrectlyFilteredProductsWithCorrespondingObjectFilter()
        {
            // Arrange
            var productRepository = new MockProductRepository();

            var predicateService = new PredicateService();
            var mostCommonWordsService = new MostCommonWordsService();
            var filterObjectService = new FilterObjectService(mostCommonWordsService);
            var highlightService = new HighlightService();

            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .BuildServiceProvider();
            var factory = serviceProvider.GetService<ILoggerFactory>();
            var logger = factory.CreateLogger<ProductsController>();

            var productController = new ProductsController(
                productRepository,
                predicateService,
                filterObjectService,
                highlightService,
                logger);

            var filterParameters = new FilterParameters
            {
                Sizes = new List<string> { "large", "small" }
            };

            // Act
            var response = await productController.Get(filterParameters);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Response>>(response);
            var createdAtActionResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var returnValue = Assert.IsType<Response>(createdAtActionResult.Value);

            var actualProduct = returnValue.Products.Single();
            var filterObject = returnValue.FilterObject;

            Assert.True(filterParameters.Sizes.All(x => actualProduct.Sizes.Contains(x)));
            Assert.Equal(40, filterObject.MaximumPrice);
            Assert.Equal(40, filterObject.MinimumPrice);
        }

        [Fact]
        public async Task GivenMaxPriceAndSizesFilterParameter_WhenGetRequestIsSend_ReturnCorrectlyFilteredProductsWithCorrespondingObjectFilter()
        {
            // Arrange
            var maxPrice = 30;

            var productRepository = new MockProductRepository();

            var predicateService = new PredicateService();
            var mostCommonWordsService = new MostCommonWordsService();
            var filterObjectService = new FilterObjectService(mostCommonWordsService);
            var highlightService = new HighlightService();

            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .BuildServiceProvider();
            var factory = serviceProvider.GetService<ILoggerFactory>();
            var logger = factory.CreateLogger<ProductsController>();

            var productController = new ProductsController(
                productRepository,
                predicateService,
                filterObjectService,
                highlightService,
                logger);

            var filterParameters = new FilterParameters
            {
                MaxPrice = maxPrice,
                Sizes = new List<string> { "large" }
            };

            // Act
            var response = await productController.Get(filterParameters);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Response>>(response);
            var createdAtActionResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var returnValue = Assert.IsType<Response>(createdAtActionResult.Value);

            var actualProducts = returnValue.Products;
            var filterObject = returnValue.FilterObject;

            Assert.True(actualProducts.All(x => x.Price <= maxPrice));
            Assert.Equal(30, filterObject.MaximumPrice);
            Assert.Equal(25, filterObject.MinimumPrice);

            var expectedSizes = new List<string>() { "large" };
            Assert.True(expectedSizes.All(x => filterObject.Sizes.Contains(x)));
        }

        [Fact]
        public async Task GivenHighlightFilterParameter_WhenGetRequestIsSend_ReturnHighlightedDescriptions()
        {
            // Arrange
            var maxPrice = 30;

            var productRepository = new MockProductRepository();

            var predicateService = new PredicateService();
            var mostCommonWordsService = new MostCommonWordsService();
            var filterObjectService = new FilterObjectService(mostCommonWordsService);
            var highlightService = new HighlightService();

            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .BuildServiceProvider();
            var factory = serviceProvider.GetService<ILoggerFactory>();
            var logger = factory.CreateLogger<ProductsController>();

            var productController = new ProductsController(
                productRepository,
                predicateService,
                filterObjectService,
                highlightService,
                logger);

            var filterParameters = new FilterParameters
            {
                Highlights = new List<string> { "this", "blue" }
            };

            // Act
            var response = await productController.Get(filterParameters);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Response>>(response);
            var createdAtActionResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var returnValue = Assert.IsType<Response>(createdAtActionResult.Value);

            var actualProducts = returnValue.Products;
            var actualDescriptions = actualProducts.Select(x => x.Description);

            var expectedHighlightedDescriptions = new List<string>
            {
                "<em>This</em> is <em>blue</em>. <em>This</em>.",
                "<em>This</em> is red.",
                "<em>Blue</em> is <em>blue</em>.",
                "<em>This</em> is red and <em>blue</em>.",
                "<em>Blue</em> is not red."
            };

            Assert.True(expectedHighlightedDescriptions.All(x => actualDescriptions.Contains(x)));
        }
    }
}
